import React, { Component } from 'react';

import StockChart from './StockChart.js';

class App extends Component {
  render() {
    return <StockChart numberOfSymbols={3}/>;
  }
}

export default App;
