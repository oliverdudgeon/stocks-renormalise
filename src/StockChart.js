import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './StockChart.css';

import {
  LineChart,
  XAxis,
  YAxis,
  Line,
  Tooltip,
} from 'recharts';

import fetch from 'node-fetch';

const func = 'TIME_SERIES_DAILY';
const apiKey = 'V8PG2RQSF0VM714B';

const ERRORS = {
  API_LIMIT_ERROR: 'Exceeded API call volume',
  INVALID_SYMBOL_ERROR: 'Invalid symbol!',
};

const TIMEOUT = 500; /* Timeout for key up events before making a fetch call */
const COLOURS = [
  '#2c3e50',
  '#2ecc71',
  '#d35400',
  '#c0392b',
  '#3498db',
  '#16a085',
  '#f39c12',
  '#f1c40f',
  '#7f8c8d',
  '#8e44ad',
];

class SymbolInputBox extends Component {
  static get propTypes() {
    return {
      onTextChange: PropTypes.func,
      number: PropTypes.number,
      error: PropTypes.string,
    };
  }

  constructor(props) {
    super(props);
    this.state = { value: '' };
    this.timeout = null;
    this.onSymbolChange = this.onSymbolChange.bind(this);
  }

  onSymbolChange(event) {
    /*
      React has it's own event wrapper which sets
      the .target.value to null. That value is needed.
    */
    event.persist();

    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.props.onTextChange(event.target.value);
    }, TIMEOUT);
  }

  render() {
    return (
      <fieldset>
        <label>Symbol {this.props.number}</label>
        <input
          type="text"
          onKeyUp={this.onSymbolChange}
          style={{ border: `1px solid ${COLOURS[this.props.number - 1]}` }}
        />
      <legend>{this.props.error}</legend>
      </fieldset>
    );
  }
}

class InputBoxes extends Component {
  static get propTypes() {
    return {
      symbols: PropTypes.array,
      updateSymbols: PropTypes.func,
      errors: PropTypes.array,
    };
  }

  render() {
    return (
      <form className="symbolInputs">
        {this.props.symbols.map((symbol, index) => (
          <SymbolInputBox
            key={index.toString()}
            number={index + 1}
            onTextChange={newSymbol => this.props.updateSymbols(newSymbol, index)}
            value={symbol}
            error={this.props.errors[index]}
          />
        ))}
      </form>
    );
  }
}


export default class StockChart extends Component {
  static get propTypes() {
    return {
      numberOfSymbols: PropTypes.number,
    };
  }

  constructor(props) {
    super(props);

    const numOfSymbols = this.props.numberOfSymbols <= 10
      && this.props.numberOfSymbols > 0 ? this.props.numberOfSymbols : 3;

    this.updateSymbols = this.updateSymbols.bind(this);

    this.state = {
      data: {},
      numberOfSymbols: numOfSymbols,
      symbols: new Array(numOfSymbols).fill(''),
      timeNormalisaionPoint: 0,
      errors: new Array(numOfSymbols).fill(''),
    };
  }

  fetchData(symbol, index) {
    fetch(`https://www.alphavantage.co/query?function=${func}&symbol=${symbol}&apikey=${apiKey}`)
      .then(response => response.json())
      .then((json) => {
        if (!('Time Series (Daily)' in json)) {
          if ('Error Message' in json) {
            throw new Error(ERRORS.INVALID_SYMBOL_ERROR);
          } else {
            throw new Error(ERRORS.API_LIMIT_ERROR);
          }
        }
        return json['Time Series (Daily)'];
      })
      .then((stockData) => {
        Object.keys(stockData).map((date) => {
          const newStockData = stockData;
          newStockData[date] = Number(stockData[date]['4. close']);
          return date;
        });
        return stockData;
      })
      .then((stockData) => {
        const { state: { data } } = this;
        const { state: { errors } } = this;

        data[symbol] = stockData;
        errors[index] = '';

        /* TODO: Change this to use update from immutability-helper */
        this.setState({ data, errors });
      })
      .catch((error) => {
        const { state: { errors } } = this;
        if (error.message === ERRORS.API_LIMIT_ERROR) {
          errors[index] = ERRORS.API_LIMIT_ERROR;
        } else if (error.message === ERRORS.INVALID_SYMBOL_ERROR) {
          errors[index] = ERRORS.INVALID_SYMBOL_ERROR;
        }

        /* TODO: Change this to use update from immutability-helper */
        this.setState({ errors });
      });
  }

  formatData() {
    /* Called in render. Do no use setState */
    const { state: { data } } = this;
    const intermediate = {};
    const formattedData = [];

    this.state.symbols.filter(s => s).forEach((symbol) => {
      if (symbol in data) {
        Object.keys(data[symbol]).forEach((date) => {
          if (date in intermediate) {
            intermediate[date][symbol] = data[symbol][date];
          } else {
            intermediate[date] = { [symbol]: data[symbol][date] };
          }
        });
      }
    });

    Object.keys(intermediate).forEach((date) => {
      formattedData.push({
        time: date,
        ...intermediate[date],
      });
    });

    return formattedData;
  }

  static percentageChange(referencePoint, point) {
    return 100 * (point - referencePoint) / referencePoint;
  }

  normaliseData(formattedData) {
    /* Called in render. Do no use setState */
    let referencePoints;
    if (this.state.timeNormalisaionPoint === 0) {
      referencePoints = [...formattedData].pop();
    } else {
      referencePoints = formattedData
        .find(point => point.time === this.state.timeNormalisaionPoint);
    }
    referencePoints = { ...referencePoints };

    return formattedData.map((point) => {
      this.state.symbols.filter(s => s).forEach((symbol) => {
        const thePoint = point;
        thePoint[symbol] = StockChart.percentageChange(referencePoints[symbol], point[symbol]);
      });
      return point;
    });
  }

  updateSymbols(newSymbol, index) {
    const { state: { symbols } } = this;

    symbols[index] = newSymbol;
    /* TODO: Change this to use update from immutability-helper */
    this.setState({ symbols });
    this.fetchData(newSymbol, index);
  }

  componentDidUpdate() {
    const { state: { symbols, data } } = this;

    Object.keys(data).forEach((symbol) => {
      if (!(symbols.includes(symbol))) {
        delete this.state.data[symbol];
      }
    });
  }

  render() {
    const formattedData = this.formatData();
    const normalisedData = this.normaliseData(formattedData);
    normalisedData.forEach((point) => {
      this.state.symbols.filter(s => s).forEach((symbol) => {
        const thePoint = point;
        thePoint[symbol] = Number(point[symbol].toFixed(2));
      });
    });

    return (
      <div className="App">
        <InputBoxes
          updateSymbols={this.updateSymbols}
          symbols={this.state.symbols}
          errors={this.state.errors}
        />
        <div className="chart">
          <LineChart
            data={normalisedData.reverse()}
            width={1000}
            height={500}
            onClick={event => event && this.setState({ timeNormalisaionPoint: event.activeLabel })}
          >
            <XAxis dataKey="time" />
            <YAxis />
            {this.state.symbols.map((symbol, index) => {
              if (symbol) {
                return (<Line
                  animationDuration={500}
                  dot={false}
                  dataKey={symbol}
                  stroke={COLOURS[index]}
                  key={symbol}
              />);
              }
              return null;
            })}
            <Tooltip/>
          </LineChart>

        </div>

      </div>
    );
  }
}
